package com.hashmap.blog.snowflake;

/**
 * An example implementation to demonstrate Snowflake Java UDF capability to parse
 * excel files.
 * 
 * The dataset used for the demonstration is sourced from :
 *  https://open.alberta.ca/opendata/new-motor-vehicle-sales
 * 
 *  An example file is present in test/data directory.
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class MotorVehicleParser {

    /**
     * This will be handler method that will get called by Snowflake's
     * dml statements.
     * 
     * @param p_dataFileName
     * @return
     * @throws IOException
     */
    public String[] parseXLS(String p_dataFileName) throws IOException {
        String dataDir = System.getProperty("com.snowflake.import_directory");
        String fPath = dataDir + p_dataFileName;
        return this.parse(fPath).toArray(String[]::new);
    }

    /**
     * Parses the header and initializes a map. the map index will reflect the column
     * number.
     * @param p_row
     * @return
     */
    private Map parseHeader(XSSFRow p_row) {
        Map<Integer, String> hdr_map = new HashMap<>();
        Iterator<Cell> cellIterator = p_row.cellIterator();
        for (int i = 0; cellIterator.hasNext(); i++) {
            Cell cell = cellIterator.next();
            hdr_map.put(i, cell.getStringCellValue().toUpperCase());
        }
        return hdr_map;
    }

    /**
     * Parses each row, converts into a JSON
     * @param p_row
     * @param p_hdr_map
     * @return
     */
    private String parseRow(final XSSFRow p_row, final Map<Integer, String> p_hdr_map) {
        Gson gsonObj = new Gson();
        Map<String, String> row_map = new HashMap<>();
        Iterator<Cell> cellIterator = p_row.cellIterator();
        for (int i = 0; cellIterator.hasNext(); i++) {
            Cell cell = cellIterator.next();

            row_map.put(p_hdr_map.get(i), "" + cell.toString());
        }
        String row_json = gsonObj.toJson(row_map);
        return row_json;
    }

    /**
     * Parses rows and stores the json into an array.
     * 
     * @param p_dataFile
     * @return
     * @throws IOException
     */
    public List<String> parse(String p_dataFile) throws IOException {
        File file = new File(p_dataFile);
        FileInputStream fIP = new FileInputStream(p_dataFile);

        // Get the workbook instance for XLSX file
        XSSFWorkbook workbook = new XSSFWorkbook(fIP);
        XSSFSheet sheet = workbook.getSheet("Data");

        Iterator<Row> rowIterator = sheet.iterator();
        Map headerMap = this.parseHeader(sheet.getRow(0));

        XSSFRow row;
        List<String> rows = new ArrayList(10);
        String rowJson;
        for(int r=0; rowIterator.hasNext(); r++) {
            row = (XSSFRow) rowIterator.next();
            if (r==0) continue;
            rowJson = this.parseRow(row, headerMap);
            rows.add(rowJson);
        }

        return rows;
    }
}
