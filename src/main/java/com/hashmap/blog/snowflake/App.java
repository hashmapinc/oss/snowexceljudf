package com.hashmap.blog.snowflake;

import java.util.List;

import com.hashmap.blog.snowflake.MotorVehicleParser;

public class App {

    
    public static void main( String[] args )
        throws Exception {
        MotorVehicleParser mvParser = new MotorVehicleParser();
        List<String> rows = mvParser.parse("./src/test/data/new-motor-vehicle-sales.xlsx");
        
        rows.forEach(System.out::println);
        
    }
}
