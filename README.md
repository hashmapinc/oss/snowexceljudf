# snowexceljudf

A Snowflake Java UDF implementation prototype to demonstrate parsing of excel file.

---
# Parsing Excel using Snowflake Java UDF
## Date : Jun-2021

In many of my engagements, clients use cases typically involved side loading data from excel files. These excel files where either maintained by their internal business users and or sourced from external data providers. So far a typical approach is to have these excel files be parsed and converted to CSV or JSON files and then hosted in Snowflake as external tables. 

Snowflake recently previewed the [Java UDF feature](https://docs.snowflake.com/en/developer-guide/udf/java/udf-java-introduction.html). So I started exploring the capabilities and want to share my findings, and a prototype implementation, around adopting the Java UDF to parse Excel files (or any other data files rather) directly in Snowflake.

### Key takeways
- Java UDF can use third party libraries, as long the functionalities are conform-ant to Snowflakes limits.
- No network or file operations allowed, other than specific imported files.
- Custom complex excel file or pdf files can be directly parsed as part of select statements.
- Reflect on how to separate the data file from the packaged jar, allowing changes in data to be observed.
- A prototype of the Java UDF implementation.

## Java UDF 
### Third-party library usage

When I came across the Snowflake Java UDF feature initially, I was like "Meh!!! so whats the big deal, we already can do looping, conditional constructs in javascript.". 

But then to ensure I got my thoughts straight I started on the documentation and understood that we can package third party libraries (like XML parsers, EXCEL parsers etc…) as part of our package. This is different from Javascript, Javascript UDF you are limited to base of what Snowflake provides.

### Reading files
Bummer!!, as per, [Following Good Security Practices](https://docs.snowflake.com/en/developer-guide/udf/java/udf-java-designing.html#following-good-security-practices) we can see that we cannot make network calls or read files from local system. This is understandable, as a measure to avoid any potential threats and security loopholes.

But, if you read between the lines the doc reflects that you can read files as long as they are part of the import statement. An example walk thru is detailed in the section [Java UDF Cookbook](https://docs.snowflake.com/en/developer-guide/udf/java/udf-java-cookbook.html#java-udf-cookbook).

## Questions & Ideation
All the above got me to thinking
- Can I host the UDF jar in an external stage, i.e. S3 bucket ?
- If we declare the excel data file, also hosted in S3, as an import; can the Java UDF still read and process the file ?
- How do we keep the UDF package (jar) separate from the data excel file ?
- If the user wants to add/update the record in the excel file and uploads the new changed file into S3, can the UDF observe the changes ?

The short answer to all the above queries, is that it is possible and they work. Interested read along the walk thru.

---

## Implementation prototype

### Dataset
We are using the "Motor-vehicle-sales" open dataset, to demonstrate parsing from a complex excel file. The dataset is stored in a separate sheet named "Data".

![](./doc/images/data_sheet.png)

### Execution
The call to the UDF will return the records as an array of json record string.
![](./doc/images/execution.png)

The array could be lateral flattened to columns as below
![](./doc/images/lateral_flattened.png)

---

### Implementation class
Apache POI, is a popular widely used java library to parse XLSX files and other document format. We can use this library to read the excel data file and return the data as an list of json records. The logic for this is implemented in the java class "com.hashmap.blog.snowflake.MotorVehicleParser".
![](./doc/images/app_output.jpg)

Following Snowflakes Java UDF documentation, we use Maven assembly plugin to package the compiled class and the various dependency into a "fat" jar. The size of the jar came out to be 40MB.

### Hosting
We host the packaged code and the excel data in an S3 bucket. The S3 bucket will be defined as an external stage in Snowflake. The data and the packaged code will be in a seperate folders.
The data will be stored inside a folder in S3 bucket. This S3 bucket will then be declared as an external stage in Snowflake.

![](./doc/images/s3_hosting_artifacts.png)

---

## Observations & Thoughts
### Package artifact size
For this prototype, the size came to be around 40MB. When the UDF gets called, Snowflake copies the artifact from the stage to the VM. Larger the size, it will take some time to initialize. Hence I would recommend to be watchful for this.

### Record count
There was 2500 or so records hence parsing and return was ok. But larger count of record could potentially cause issue. Most of my clients data recount from excel are typically on this count or less, but would recommend to be watchful on this too.

### Change or updates to EXCEL data file
If a change is made to excel, the UDF is able to observe the changed records. However, the filename should be the same original data file name. Just because you can pass the file name as a parameter, does not mean you can read any files from the stage. The files are constrained to the list of files defined in the import statement of the create function call.

### Copy and Store
If the excel file does not change often, I guess it would be better performant to retrieve and store the data into a transient table. Further data processing would refer to this transient table for various data processing needs.

### Development experience
I got to say, in the past developing javascript udf was simple but cumbersome. As there was no good IDE and you always needs to run a select/DML operations to test the code. Not able to import libraries means I cannot also develop common modules outside, resulting in copies and copies of the same code across multiple javascript UDF.
With the Java UDF, I have the ability to develop locally using an IDE of choice (visual code todays flavour!!). I was able to package using Maven and there is potential to perform unit test using junit or other framework. Code review, documentation etc.. Hence me likeeee!!!

---

## Other Java UDF usage scenarios
Based of the above, I believe the following usage patterns can be adopted using Java UDF.
- Parsing PDF, Custom file formats which contains data.
- Simple ML predictive determination, as the model could be packaged with the jars.
- Complex mathematical calculations which does not fit inside a javascript procedure.

---

## Meh!! to NICE
What started of as a Meh, turned into a NICE, based of the experiment above and the potential usage scenarios. I hope there is a light bulb turning on your thoughts for a HA HA moment.

Regardless this Java UDF is a feature I wish will expand more and definitely a stay.